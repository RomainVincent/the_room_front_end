import React, { Component } from 'react'
import ChatInput from './ChatInput'
import ChatMessage from './ChatMessage'

const URL = 'ws://the-room-back-end-staging.herokuapp.com'

class Webchat extends Component {
    state = {
        messages: [],
    }

    ws = new WebSocket(URL)

    componentDidMount() {
        this.ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            //console.log('connected')
        }

        this.ws.onmessage = evt => {
            // on receiving a message, add it to the list of messages
            const message = JSON.parse(evt.data)
            //this.addMessage(message)
            //console.log(evt.data);
            $('.chatbox').append('<div class="chatbox-message"><div class="message-out"> <span class="message-nickname">' + message.name + ' : </span>' + message.message + '</div></div>');
        }

        this.ws.onclose = () => {
            //console.log('disconnected')
            // automatically try to reconnect on connection loss
            this.setState({
                ws: new WebSocket(URL),
            })
        }
    }

    addMessage = message =>
        this.setState(state => ({ messages: [message, ...state.messages] }))

    submitMessage = messageString => {
        // on submitting the ChatInput form, send the message, add it to the list and reset the input
        const message = { name: $('#name').val(), message: messageString }
        this.ws.send(JSON.stringify(message))
        //this.addMessage(message)

        $('.chatbox').append('<div class="chatbox-message"><div class="message-in"> <span class="message-nickname">' + $('.nickname').text() + ' : </span>' + messageString + '</div></div>');
    }

    render() {
        return (
            <div>
                <label htmlFor="name" className="nickname-chat">
                    Name:&nbsp;
                    <input
                        className="form-control"
                        type="text"
                        id={'name'}
                        placeholder={'Enter your name...'}
                        value=""
                        onChange={e => this.setState({ name: e.target.value })}
                    />
                </label>
                <ChatInput
                    ws={this.ws}
                    onSubmitMessage={messageString => this.submitMessage(messageString)}
                />
                {this.state.messages.map((message, index) =>
                    <ChatMessage
                        key={index}
                        message={message.message}
                        name={message.name}
                    />,
                )}
            </div>
        )
    }
}

export default Webchat
