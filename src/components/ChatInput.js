import React, { Component } from 'react'
import PropTypes from 'prop-types'

class ChatInput extends Component {
    static propTypes = {
        onSubmitMessage: PropTypes.func.isRequired,
    }
    state = {
        message: '',
    }

    render() {
        return (
            <form
                action="."
                onSubmit={e => {
                    e.preventDefault()
                    this.props.onSubmitMessage(this.state.message)
                    this.setState({ message: '' })
                }}
            >
                <div className="chat-bottom">
                    <div className="message-block">
                        <input type="text" className="input-message" placeholder="Type a message"  value={this.state.message}
                               onChange={e => this.setState({ message: e.target.value })} />
                        <button className="btn-message" type="submit"><i className="fas fa-paper-plane"></i></button>
                    </div>
                </div>
            </form>
        )
    }
}

export default ChatInput
