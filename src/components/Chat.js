import React from "react";
import '../styles/chat.css';
import Webchat from './Webchat';

class Chat extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            user: {

            }
        };
    }

    handleSidebar() {
        $('#sidebar').toggleClass('active');
    }

    // Secure access to chat
    UNSAFE_componentWillMount() {
        if (sessionStorage.getItem('token') && sessionStorage.getItem('userID')) {
            const token = sessionStorage.getItem('token');
            const userID = sessionStorage.getItem('userID');
            const cl = this;

            const settings = {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            }

            fetch(`https://the-room-back-end-staging.herokuapp.com/users/` + userID, settings)
                .then(response => response.json())
                .then(data => {
                    switch (data.code) {
                        case 200:
                            //console.log(data);
                            cl.setState({user: data.user});
                            $('#name').val(this.state.user.username);
                            break
                        default:
                            this.props.history.push("/")
                            break
                    }
                }).catch(e => {
                this.props.history.push("/")
            });
        } else {
            this.props.history.push("/")
        }
    }

    render() {
        const ajust = { 'marginLeft': '6px' };
        return (
            <div>
                <nav className="navbar navbar-expand-sm navbar-custom">
                    <ul className="navbar-nav mr-auto" style={ajust}>
                        <li className="nav-item" onClick={this.handleSidebar}>
                            <i className="fas fa-bars fa-2x"></i>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a className="nav-link">
                                <img src="../styles/img/avatar_male.png" alt="Avatar" className="avatar img-responsive" />
                                <span className="nickname"> {this.state.user.username} </span>
                                <small className="email-small"> {this.state.user.email} </small>
                            </a>
                        </li>
                    </ul>
                </nav>

                <div className="wrapper">
                    <nav id="sidebar">
                        <div className="sidebar-header">

                            <li className="nav-item list-unstyled active">
                                <i className="fas fa-comments fa-2x"></i>
                            </li>

                            <li className="nav-item list-unstyled">
                                <i className="fas fa-users fa-2x"></i>
                            </li>

                            <li className="nav-item list-unstyled">
                                <i className="fas fa-envelope fa-2x"></i>
                            </li>

                            <li className="nav-item list-unstyled">
                                <i className="fas fa-user fa-2x"></i>
                            </li>
                        </div>
                    </nav>

                    <div className="container pt-4">
                        <h1 className="display-4 section-title">General chat</h1>
                        <div className="chatbox"> </div>
                        <Webchat />
                    </div>

                </div>
            </div>
        );
    }
}

export default Chat;
